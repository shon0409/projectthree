#include <iostream>
#include "Vector.h"

using std::cout;
using std::endl;

/* c'tor of vector
input: starter value for capacity
output: none
*/
Vector::Vector(int n)
{
	if (n < MIN_SIZE)
	{
		n = MIN_SIZE;
	}
	_elements = new int[n];
	_size = 0;
	_capacity = n;
	_resizeFactor = n;
}

/* d'tor for vector, cleans the memory from _elements
input: none
output: none
*/
Vector::~Vector()
{
	delete[] _elements;
}

/* getter for size of vector
input: none
output: size of vector
*/
int Vector::size() const
{
	return _size;
}

/* getter for capacity of vector
input: none
output: capacity of vector
*/
int Vector::capacity() const
{
	return _capacity;
}

/* getter for resize factor of vector
input: none
output: resize factor of vector
*/
int Vector::resizeFactor() const
{
	return _resizeFactor;
}

/* checks if the vector is empty or not
input : none
output: empty or not
*/
bool Vector::empty() const
{
	return (_size == 0);
}

/* adds another item in the back end of the vector
input: val to push
output: none
*/
void Vector::push_back(const int & val)
{
	int* temp = nullptr;
	int i = 0;
	if (_size == _capacity)
	{
		temp = new int[_capacity + 1];
		for (i = 0; i < _size; ++i)
		{
			temp[i] = _elements[i];
		}
		delete _elements;
		_elements = temp;
		++_capacity;
	}
	_elements[_size] = val;
	++_size;
}

/* deletes the last item in vector
input: none
output: the last item deleted
*/
int Vector::pop_back()
{
	int last = 0;
	if (empty())
	{
		cout << "error:pop from empty vector" << endl;
		return -9999;
	}
	last = _elements[_size - 1];
	--_size;
	return last;
}

/* allocate memory for vector
input: room to allocate
output: none
*/
void Vector::reserve(int n)
{
	int* temp = nullptr;
	int i = 0;
	if (_capacity < n)
	{
		while (_capacity < n)
		{
			_capacity += _resizeFactor;
		}
		temp = new int[_capacity];
		for (i = 0; i < _size; ++i)
		{
			temp[i] = _elements[i];
		}
		delete _elements;
		_elements = temp;
	}
}

/* change the size of the vector
input: new size
output: none
*/
void Vector::resize(int n)
{
	if (n <= _capacity)
	{
		_size = n;
	}
	else if (n > _capacity)
	{
		reserve(n);
		_size = n;
	}
}

/* puts a value in every cell in vector
input: value to enter
output: none
*/
void Vector::assign(int val)
{
	int i = 0;
	for (i = 0; i < _capacity; ++i)
	{
		_elements[i] = val;
	}
	_size = _capacity;
}

/* change the size of a given vector and make all the new space a given value
input: new size, value to enter
output: none
*/
void Vector::resize(int n, const int & val)
{
	int i = 0;
	if (n <= _capacity)
	{
		if (_size < n)
		{
			for (i = _size; i < n; ++i)
			{
				_elements[i] = val;
			}
		}
		_size = n;
	}
	else if (n > _capacity)
	{
		reserve(n);
		for (i = _size; i < _capacity; ++i)
		{
			_elements[i] = val;
		}
		_size = n;
	}
}

/* copy c'tor for vector
input: other vector to copy
output: none
*/
Vector::Vector(const Vector & other)
{
	*this = other;
}

/* the operator = for vector
input: vector on the other side of =
output: the copied vector
*/
Vector & Vector::operator=(const Vector & other)
{
	int i = 0;
	delete[] _elements;
	_size = other._size;
	_capacity = other._capacity;
	_resizeFactor = other._resizeFactor;
	_elements = new int[_capacity];
	for (i = 0; i < _size; ++i)
	{
		_elements[i] = other._elements[i];
	}
	return *this;
}

/* the operator [] for vector
input: index in []
output: refrence to this->_elements[n]
*/
int & Vector::operator[](int n) const
{
	if (n >= _capacity)
	{
		cout << "The index is out of range" << endl;
		return _elements[0];
	}
	int& ref = _elements[n];
	return ref;
}

